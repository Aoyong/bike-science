import re
import json
import pandas as pd
from shapely.geometry import Point
import geopandas as gpd


def fix_name(name):
    res = re.search('\\d\\d\\d - (.+)', name)
    if res == None: return name
    return res.group(1)


def load_stations_data(stations_file):
    # http://api.citybik.es/v2/networks/ecobici-buenos-aires
    stations_file = open(stations_file)
    stations_json = json.loads(stations_file.read())['network']['stations']
    stations_file.close()

    names = []
    lats = []
    longs = []
    for s in stations_json:
        if s['longitude'] < -59 or s['longitude'] > -57 or s['latitude'] > -33: continue
        names.append(fix_name(s['name']))
        lats.append(s['latitude'])
        longs.append(s['longitude'])
        
    return names, lats, longs


def stations_df(stations_file):
    names, lats, longs = load_stations_data(stations_file)
    stations = pd.DataFrame(data={'name': names, 'lat': lats, 'lon': longs})
    return stations


def stations_geodf(stations_file):
    names, lats, longs = load_stations_data(stations_file)
    geometries = []
    for lat, lon in zip(lats, longs):
        geometries.append(Point(lon, lat))
    return gpd.GeoDataFrame(data={'name': names}, geometry=geometries, crs={'init': 'epsg:4326'})