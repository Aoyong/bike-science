import sys
sys.path.append("..")
from bikescience import grid

BA_LAT = -34.61315
BA_LON = -58.37723

def create(n=40, 
           west_offset = -0.142,
           east_offset = 0.05,
           north_offset = 0.084,
           south_offset = -0.079):
    return grid.Grid(n, 
                     west_limit=BA_LON + west_offset,
                     east_limit=BA_LON + east_offset,
                     north_limit=BA_LAT + north_offset,
                     south_limit=BA_LAT + south_offset)                     