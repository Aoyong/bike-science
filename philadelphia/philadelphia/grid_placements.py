import geopandas as gpd
import pandas as pd

from . import grid as gr

H_DISPLACEMENT = 0.004
V_DISPLACEMENT = 0.003

placement1 = gr.create().geodataframe()
placement2 = gr.create(h_displacement=-H_DISPLACEMENT, v_displacement=V_DISPLACEMENT).geodataframe()
