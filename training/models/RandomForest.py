from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestRegressor
import numpy as np

def fit(X_train, y_train):
    param_grid = {
        'n_estimators': [10, 20],
        'max_features': ['auto'],
        'max_depth': [50, 100, 200],
        'criterion': ['mae'],
    }
    print('Param grid:', param_grid)
    grid_search = GridSearchCV(estimator=RandomForestRegressor(), param_grid=param_grid, cv=5, n_jobs=-1, verbose=2)
    grid_search.fit(X_train, y_train)
    return grid_search.best_estimator_, grid_search.best_params_
    
def base_fit(X_train, y_train):
    base_model = RandomForestRegressor(n_estimators=10, random_state=42)
    base_model.fit(X_train, y_train)
    return base_model
