from sys import argv
import pandas as pd
from sklearn.model_selection import train_test_split
import importlib
import models
import numpy as np
from datetime import datetime as dt
import joblib
import matplotlib.pyplot as plt
import random
import gc


def evaluate(model, test_features, test_labels):
    predictions = model.predict(test_features)
    errors = abs(predictions - test_labels)
    mape = 100 * np.mean(errors / test_labels)
    accuracy = 100 - mape
    print('Average Error: {:0.4f}'.format(np.mean(errors)))
    print('Accuracy: {:0.2f}%'.format(accuracy))
    return predictions


if len(argv) < 4:
    print('USAGE: python3 training.py ModelName X_file y_file')
    exit()
    
model = importlib.import_module('.' + argv[1], 'models')
    
X = pd.read_csv(argv[2])
y = pd.read_csv(argv[3])

sampling = random.sample(y.index.tolist(), len(X) // 4)
X = X.ix[sampling]
y = y.ix[sampling]

del sampling
gc.collect()

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=42)

now = dt.now().strftime('%m-%d-%Y %H:%M')
print()
print()
print(now)
print('------------------------------------------------------------')
print(argv[1])
print()
print('Base model:')
base_estimator = model.base_fit(X_train, y_train[y_train.columns[0]])
evaluate(base_estimator, X_test, y_test[y_test.columns[0]])
print()
print('Tested model:')
best_estimator, best_params = model.fit(X_train, y_train[y_train.columns[0]])
print(best_params)
predictions = evaluate(best_estimator, X_test, y_test[y_test.columns[0]])
print()
print('End of training at', dt.now().strftime('%m-%d-%Y %H:%M'))
joblib.dump(best_estimator, argv[1] + '-' + now + '.joblib')

plt.figure(figsize=(10, 10))
plt.scatter(y_test[y_test.columns[0]], predictions)
plt.xlabel('labels')
plt.ylabel('predictions')
plt.savefig(argv[1] + '-' + now + '.png')
