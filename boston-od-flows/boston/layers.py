import pandas as pd
import geopandas as gpd
import folium
import fiona
import json
import numpy as np
import math
from bikescience.flow import od_countings, flow_map

def draw_points(fmap,points,point_color,radius_size):
    for index, row in points.iterrows():
        folium.CircleMarker(location=[row.latitude, row.longitude], radius=radius_size,
                            popup="", color=point_color).add_to(fmap)

def switch_fac_type(fac_type):
    '''
    This module is to be used when there are more than one type of bike lane using different colors
    
    fac_types: facility types from the bike path shapefile [1-9]
    1 - blue: protected/painted/dedicated bike lanes
    9 - yellow: painted/bike-friendly/car-shared bike lanes
    5/7 - green: trail/bike-friendly/ped-shared bike trails
    2/4 - red: planned/removed bike lanes (some of them already exists in Google Maps)
    3/6/8 - black: insignificant, almost nothing in the Greater Boston area
    '''
    
    switcher = {
        1: 'blue',
        2: 'red',
        3: 'black',
        4: 'red',
        5: 'green',
        6: 'black',
        7: 'green',
        8: 'black',
        9: 'orange'
    }
    return switcher.get(fac_type,'white')


def point_layer(fmap,points,filename='',point_color='red',radius_size=5):
    '''
    Creates a layer with points in a folium map
    
    Parameters
    fmap: folium map that receives the layer
    points: a Dataframe with stops, including latitude, longitude and a name
    filename: if not null, save the map in this file
    point_color: a color for the points
    radius_size: point radius
    '''
    
    draw_points(fmap,points,point_color,radius_size)
    
    if filename:
        fmap.save(filename)
    return fmap

def path_layer(fmap,paths,filename='',layer_color='red'):
    '''
    Creates a layer with paths in a folium map
    
    Parameters
    fmap: folium map that receives the layer
    paths: a GeoDataFrame with paths
    filename: if not null, save the map in this file
    color: choose a color for the paths
    '''
    
    style_function=lambda style:{'color':layer_color}
    
    folium.GeoJson(paths,style_function=style_function).add_to(fmap)
    
    if filename:
        fmap.save(filename)
    return fmap

def path_smulti_layer(fmap,paths,filename=''):
    '''
    Creates a layer with different bike paths in a folium map
    
    Parameters
    fmap: folium map that receives the layer
    paths: a GeoDataFrame with paths
    filename: if not null, save the map in this file
    '''
    
    style_function = lambda xcolor:{'color': switch_fac_type(xcolor['properties']['fac_type']),'opacity':1.0}

    folium.GeoJson(paths,style_function=style_function).add_to(fmap)

    if filename:
        fmap.save(filename)
    return fmap
