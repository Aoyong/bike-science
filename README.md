# BikeScience

Bike Science is a collaborative open source research project between the [MIT Senseable City Lab](http://senseable.mit.edu/) and the [InterSCity](http://interscity.org/) team at the University of São Paulo.
The project relies on advanced Data Science techniques for investigating Bike-Sharing Systems around the world. 

## Getting started

BikeScience is a data science tool that allows urban planners to analyze and understand the impact of urban bicycling in their cities. 
The tool uses data from Bike-Sharing Systems and travel surveys to analyze patterns of bike usage. Moreover, other data sources (e.g., bike infrastructure, accident records) can be added in BikeScience to improve the analyses.
Such knowledge can be used to support future planning and improvements regarding bicycling. 

BikeScience contains several notebooks for several analyses: 
- **maps** showing the main bike trip flows in a region of interest
- **route suggestions** between origin and destination zones
- **statistics** about the use of Bike-Sharing Systems
- **city zones** where there are more trips
- **heatmaps** showing bike stations that are more used, and so on. 

Using BikeScience you can investigate any other interesting issues you may want to know about. 
For example, in the paper below, BikeScience was used to compare bicycling and pedestrian mobility patterns in Greater Boston: 

Christian Bongiorno, Daniele Santucci, Fabio Kon, Paolo Santi, and Carlo Ratti. [Comparing bicycling and pedestrian mobility: Patterns of non-motorized human mobility in Greater Boston](https://interscity.org/assets/journal_of_transport_geography_2019.pdf). Journal of Transport Geography, Volume 80, October 2019.

The instructions below explain the basic requirements to get start with the project. 

## Prerequisites

This project is being developed with Python 3.

### Native libraries

* `libspatialindex`

```bash
sudo apt-get install libspatialindex-c4v5
```

### Python packages

* seaborn
* matplotlib
* folium
* pandas
* geopandas
* rtree
* jupyter (or jupyterlab)
* numpy 
* scipy 
* sklearn 
* holidays
* h3 (depends on cc, make, and cmake)

You can use [pip](https://pypi.org/project/pip/) or [conda](https://conda.io/) to install these python packages.

```bash
conda install folium
```

## Usage

You can use the current notebooks in the **boston-od-trips** folder to see examples of analyses we've already done. 
* **Descriptive-Statistics** analyzes bycicle trips of the Boston's bike-sharing system regarding *average use over the years*, *age*, *trip duration*, *speed*, and *distance*.
* **Flows** shows the most common bicycle flows for different periods of a day.
* **Layers** relates bycicle flows with the infrastructures of bicycle and public transportation of the Greater Boston area.
* **Hub-Analysis** shows regions of the cities in which there are more starts ans ends of bike trips.
* **Distance-Analysis** explores trips by their distances.
* **Fast-Cyclists** outlines a profile of speeders, which are more exposed to accidents.


## License

This project is licensed under the [MIT License](https://opensource.org/licenses/MIT) - see the LICENSE file for details.

## Contact

If you any questions, comments, or suggestions about the tool feel free to contact us:

- hamario at ime dot usp dot br
