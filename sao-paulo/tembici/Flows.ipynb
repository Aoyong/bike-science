{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bicycle Mobility Flow Analysis\n",
    "\n",
    "## TemBici - São Paulo\n",
    "\n",
    "Our approach is based on dividing the city into homogeneous regions by using a uniform **grid** and counting the number of bike trips from one grid cell to the other (called here a **flow**). We draw directed arrows to show flow direction and adjust the origin and end point of flows according to a weighted average based on the dock station usage for that specific flow.\n",
    "\n",
    "The raw amount of flows within a city is very large. Showing all of them to a user is overwhelming and does not allow any reasonable analysis. To show this information in a comprehensible manner, we divide the flows in **tiers**. For instance, dividing the flows into 4 tiers, each one will contain 25\\% of the trips.\n",
    "\n",
    "In addition to the mobility flows, another relevant information is what regions of the city are the major hubs initiating or ending bike trips. The **hub** analysis map shows dark green markers in the top regions of the city where bike trips start and dark red markers in the top regions where trips end. Light green markers and light red markers point to second tier hubs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "hide_input": true
   },
   "outputs": [],
   "source": [
    "# Libraries\n",
    "\n",
    "import tembici.load_trips as tr\n",
    "import tembici.stations as st\n",
    "\n",
    "import bikescience.sp_grid as gr\n",
    "from bikescience.stations import draw_stations\n",
    "import bikescience.interface as interf\n",
    "import bikescience.tiers as tiers\n",
    "import bikescience.load_trips as btr\n",
    "import bikescience.flow as flow\n",
    "\n",
    "import pandas as pd\n",
    "from ipywidgets import interact_manual, widgets\n",
    "from IPython.core.display import display, HTML\n",
    "import folium\n",
    "from folium.plugins import HeatMap"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Input data:\n",
    "* all trips files\n",
    "* bike stations\n",
    "* distances between stations (calculated by [GraphHopper](https://www.graphhopper.com/) service)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "hide_input": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1684433 trips\n"
     ]
    }
   ],
   "source": [
    "trips = tr.load_trips_files('../../../tembici/trips_*.csv')\n",
    "print(len(trips), 'trips')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "hide_input": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "235 stations in São Paulo\n"
     ]
    }
   ],
   "source": [
    "stations = pd.read_csv('../../../tembici/Estações_Tembici_fev2019.csv')\n",
    "stations = stations[stations.project == 'BikeSampa']\n",
    "stations = st.stations_geodf(stations)\n",
    "print(len(stations), 'stations in São Paulo')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "hide_input": true
   },
   "outputs": [],
   "source": [
    "stations_distances = pd.read_csv('../../data/sao-paulo/bike-stations/stations_distances.csv')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Coverage area\n",
    "\n",
    "* grid cells are origin and destination areas (granularity of flows)\n",
    "* each **flow** is a set of trips, from one cell to another\n",
    "* the default coverage area embraces the stations (black dots) at the time of our study\n",
    "  * you may adjust it as you have more data and new stations are brought into operation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "hide_input": true
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "038fc9ba45c4417cb26c9da96bea6653",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(FloatSlider(value=-0.11, description='west_delta', layout=Layout(width='50%'), max=0.5, …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "# default\n",
    "grid = gr.create(n=50, \n",
    "                 west_offset=-0.11, east_offset=0.05, north_offset=0.05, south_offset=-0.1)\n",
    "\n",
    "def set_grid_limits(west_delta, east_delta, north_delta, south_delta, grid_size):\n",
    "    global grid\n",
    "    grid = gr.create(n=grid_size, \n",
    "                     west_offset=west_delta, east_offset=east_delta, \n",
    "                     north_offset=north_delta, south_offset=south_delta)\n",
    "    fmap = grid.map_around(zoom=12)\n",
    "    draw_stations(fmap, stations, 'name')\n",
    "    folium.Marker([gr.SP_LAT, gr.SP_LON]).add_to(fmap)\n",
    "    display(fmap)\n",
    "    \n",
    "im = interact_manual(\n",
    "    set_grid_limits,\n",
    "    west_delta=interf.grid_delta_selector(-0.11, -0.5, 0.5),\n",
    "    east_delta=interf.grid_delta_selector(0.05, -0.5, 0.5),\n",
    "    north_delta=interf.grid_delta_selector(0.05, -0.5, 0.5),\n",
    "    south_delta=interf.grid_delta_selector(-0.1, -0.5, 0.5),\n",
    "    grid_size=widgets.SelectionSlider(options=[10, 20, 30, 40, 50, 60, 70, 80, 90, 100], value=50)\n",
    ")\n",
    "im.widget.children[5].description = 'Show grid'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Flow map and tiers\n",
    "\n",
    "#### Map\n",
    "\n",
    "Bike flows at different levels of granularity\n",
    "\n",
    "#### Tiers  \n",
    "\n",
    "Distribution of flows of trips across _N_ tiers. Flows are:\n",
    "* ordered by number of trips, descending\n",
    "* broken into _N_ quantiles\n",
    "* each quantile has some information summarized"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "hide_input": true
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "587fd18f2b324b59ac3e955512a1091d",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(SelectionRangeSlider(continuous_update=False, description='Trip period', index=(0, 15), …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def show_map(period, days, period_of_day, distance, tier):\n",
    "    fmap = grid.map_around(zoom=13)\n",
    "    print('Calculating...')\n",
    "    \n",
    "    start, end = interf.period_interval(period)\n",
    "    trips_filter = btr.day_functions[days](trips)\n",
    "    trips_filter = btr.period_functions[period_of_day](trips_filter)\n",
    "    trips_filter = trips_filter[(trips_filter['starttime'] >= start) & (trips_filter['starttime'] < end)]\n",
    "\n",
    "    if distance in [1, 2]:\n",
    "        trips_filter = st.merge_trips_and_stations(trips_filter, stations)\n",
    "        trips_filter = st.merge_trips_stations_and_distances(trips_filter, stations_distances)\n",
    "        if distance == 1:\n",
    "            trips_filter = trips_filter[trips_filter['distance'] < 1]\n",
    "        else:\n",
    "            trips_filter = trips_filter[trips_filter['distance'] > 4]\n",
    "        \n",
    "    if len(trips_filter) == 0:\n",
    "        print('No trips found.')\n",
    "        return\n",
    "            \n",
    "    od = flow.od_countings(trips_filter, grid, stations,\n",
    "                           station_index='name', \n",
    "                           start_station_index='start_station_name', \n",
    "                           end_station_index='end_station_name')\n",
    "    flow.draw_stations(fmap, stations, 'name')\n",
    "    \n",
    "    tiers_table, _ = tiers.separate_into_tiers(od.sort_values('trip counts', ascending=False), trips_filter, None, \n",
    "                                               max_tiers=4)\n",
    "    display(tiers_table)\n",
    "    if tier > 0:\n",
    "        tiers_row = tiers_table[tiers_table['tier'] == tier]\n",
    "        tiers_row = tiers_row.loc[tiers_row.index[0]]\n",
    "        flow.flow_map(fmap, od, grid, stations, minimum=tiers_row['min'], maximum=tiers_row['top'], radius=2.0)\n",
    "    else:\n",
    "        tiers_row = tiers_table[tiers_table['tier'] == 2]\n",
    "        tiers_row = tiers_row.loc[tiers_row.index[0]]\n",
    "        flow.flow_map(fmap, od, grid, stations, minimum=tiers_row['min'], radius=2.0)\n",
    "        \n",
    "    print('Done.')\n",
    "    file = 'maps/flows.html'\n",
    "    fmap.save(file)\n",
    "    display(HTML('Saved at <a href=\"' + file + '\" target=\"_blank\">' + file + '</a>'))\n",
    "    display(fmap)\n",
    "\n",
    "flow.N = 20\n",
    "im = interact_manual(\n",
    "    show_map,\n",
    "    period=interf.period_selector(trips, index=(0, 15)),\n",
    "    days=widgets.Dropdown(options=[('all', 0), ('working days', 1), ('weekends', 2), ('holidays', 3), \n",
    "                                   ('weekends + holidays', 4)], value=1),\n",
    "    period_of_day=widgets.Dropdown(options=[('all', 0), ('morning', 1), ('lunchtime', 2), ('afternoon', 3)],\n",
    "                                   value=1),\n",
    "    distance=widgets.Dropdown(options=[('all', 0), ('< 1Km', 1), ('> 4Km', 2)], value=0),\n",
    "    tier=widgets.Dropdown(options=[('4', 4), ('3', 3), ('2', 2), ('all', 0)], value=4)\n",
    ")\n",
    "im.widget.children[5].description = 'Show map' "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hub analysis\n",
    "\n",
    "Concentration of trip starts and ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "hide_input": true
   },
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "ae9e927f7606423abffc18b8fd1bd6ab",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "interactive(children=(SelectionRangeSlider(continuous_update=False, description='Trip period', index=(0, 15), …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "def show_heatmap(period, trip_point, days, period_of_day):\n",
    "    print('Calculating...')\n",
    "    heatmap = folium.Map([(grid.north_limit+grid.south_limit)/2, (grid.west_limit+grid.east_limit)/2], \n",
    "                         zoom_start=12, tiles='stamentoner')\n",
    "    \n",
    "    start, end = interf.period_interval(period)\n",
    "    trips_filter = btr.day_functions[days](trips)\n",
    "    trips_filter = btr.period_functions[period_of_day](trips_filter)\n",
    "    trips_filter = trips_filter[(trips_filter['starttime'] >= start) & (trips_filter['starttime'] < end)]\n",
    "    trips_filter = st.merge_trips_and_stations(trips_filter, stations)\n",
    "    \n",
    "    heat_data = [[row['lat_' + trip_point], row['lon_' + trip_point]] \n",
    "                 for index, row in trips_filter.iterrows()]\n",
    "    HeatMap(heat_data, blur=25, max_val=40, min_opacity=0.6).add_to(heatmap)\n",
    "    heatmap.save(\"maps/heatmap.html\")\n",
    "    print('Done.')\n",
    "    display(heatmap)\n",
    "\n",
    "im = interact_manual(\n",
    "    show_heatmap,\n",
    "    period=interf.period_selector(trips, index=(0, 15)), \n",
    "    trip_point=widgets.RadioButtons(options=['start', 'end']),\n",
    "    days=widgets.Dropdown(options=[('all', 0), ('working days', 1), ('weekend', 2)], value=1),\n",
    "    period_of_day=widgets.Dropdown(options=[('all', 0), ('morning', 1), ('lunchtime', 2), ('afternoon', 3)],\n",
    "                                   value=1)\n",
    ")\n",
    "im.widget.children[4].description = 'Show map'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
