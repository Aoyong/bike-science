import pandas as pd
import json
import datetime as dt
from pytz import timezone

def extract_start_times(trips):
    def extract_fn(ts_str):
        ts = json.loads(ts_str)[0]
        gmt_time = dt.datetime.utcfromtimestamp(ts)
        gmt_time = timezone('UTC').localize(gmt_time)
        sp_time = gmt_time.astimezone(timezone('America/Sao_Paulo'))
        return sp_time.replace(tzinfo=None)
    return trips.timestamps.apply(extract_fn)