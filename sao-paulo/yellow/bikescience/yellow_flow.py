import json
from shapely.geometry import Point
import pandas as pd
import geopandas as gpd

from . import flow


def extract_points(row):
    lats = json.loads(row.lats)
    if len(lats) < 2:
        return None, None, None, None, None, None
    longs = json.loads(row.longs)
    origin = Point(longs[0], lats[0])
    destination = Point(longs[-1], lats[-1])
    return origin, destination, lats[0], longs[0], lats[-1], longs[-1]


def merge_trips_and_cells(trips, grid):
    trips[['origin', 'destination', 'start_lat', 'start_lon', 'end_lat', 'end_lon']] = \
            trips.apply(extract_points, axis=1, result_type='expand')
    trips = trips[~trips.origin.isnull()]
    origins = gpd.GeoDataFrame(trips.tripid, geometry=trips.origin)
    destinations = gpd.GeoDataFrame(trips.tripid, geometry=trips.destination)
    origins_cells = gpd.sjoin(grid.geodataframe(), origins, op='contains')
    destinations_cells = gpd.sjoin(grid.geodataframe(), destinations, op='contains')
    trips_and_cells = trips.merge(origins_cells, on='tripid').merge(destinations_cells, on='tripid')
    trips_and_cells = trips_and_cells[['tripid', 'i_x', 'j_x', 'i_y', 'j_y', 
                                       'start_lat', 'start_lon', 'end_lat', 'end_lon']]
    trips_and_cells.columns = ['trip counts', 'i_start', 'j_start', 'i_end', 'j_end', 
                               'start_lat', 'start_lon', 'end_lat', 'end_lon']
    return trips_and_cells

    
def od_countings(trips, grid):
    with_od_cells = merge_trips_and_cells(trips, grid)
    with_od_cells['trip counts'] = 1
    return flow.weighted_counts(with_od_cells,  ['i_start', 'j_start', 'i_end', 'j_end'])
